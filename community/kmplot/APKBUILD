# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kmplot
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://edu.kde.org/kmplot"
pkgdesc="Mathematical Function Plotter"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kguiaddons-dev
	ki18n-dev
	kparts-dev
	kwidgetsaddons-dev
	qt6-qtbase-dev
	qt6-qtsvg-dev
	samurai
	"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/education/kmplot.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmplot-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DBUILD_WITH_QT6=ON \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
053b7b3c67b4cfc17b6def98aa6b3ce44ca0a7c4fef05fe849f0b79b8713f7c9f4850ccdf9a81deecd5270e29bdd59093f011c9db6c0f6d4dc929dab4eb1c1ed  kmplot-24.08.0.tar.xz
"
