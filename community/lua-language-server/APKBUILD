# Maintainer: Jordan Christiansen <xordspar0@gmail.com>
pkgname=lua-language-server
pkgver=3.10.5
pkgrel=0
pkgdesc="Language Server for Lua"
url="https://github.com/LuaLS/lua-language-server"
# s390x/ppc64le: ftbfs
arch="all !s390x !ppc64le"
license="MIT"
makedepends="bash linux-headers samurai"
subpackages="$pkgname-doc"
source="https://github.com/LuaLS/lua-language-server/archive/refs/tags/$pkgver/lua-language-server-$pkgver.tar.gz
	lua-language-server-submodules-$pkgver.zip.noauto::https://github.com/LuaLS/lua-language-server/releases/download/$pkgver/lua-language-server-$pkgver-submodules.zip
	wrapper
	"

prepare() {
	unzip -o "$srcdir"/lua-language-server-submodules-$pkgver.zip.noauto \
		-d "$builddir"
	default_prepare
}

build() {
	ninja -C 3rd/luamake -f compile/ninja/linux.ninja
	./3rd/luamake/luamake all
}

check() {
	./3rd/luamake/luamake unit-test
}

package() {
	install -Dm755 "$srcdir"/wrapper "$pkgdir"/usr/bin/lua-language-server
	install -Dm755 bin/lua-language-server \
		-t "$pkgdir"/usr/lib/lua-language-server/bin
	install -Dm644 bin/main.lua \
		-t "$pkgdir"/usr/lib/lua-language-server/bin
	install -Dm644 debugger.lua main.lua \
		-t "$pkgdir"/usr/lib/lua-language-server
	cp -a locale meta script "$pkgdir"/usr/lib/lua-language-server

	install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

sha512sums="
3b30e85f77e56896f55a6c46c08f60f06f0fd67f113447ec3319eba7a6146d73467f7ade33ab07a8e689c67767713a89438a3ed1565949d26dfa45017d7b60cd  lua-language-server-3.10.5.tar.gz
9197382d1fa7ae213134415a7e09847977b06752b63c942948fc6d2bc11d3a8532e318a3ed547bec9763c6f74753ba422e3029d6ca328252ea9ba01892ec41e6  lua-language-server-submodules-3.10.5.zip.noauto
d8d34d2ae8073c256b5f98f4cc7db058fbb92d63a5709894fca898ab47fcfcfca2d1419a1060c29464fbad937e3d09f0cde404b4d98609eec934ea4392044849  wrapper
"
