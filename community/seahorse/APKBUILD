# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=seahorse
pkgver=43.0
pkgrel=5
pkgdesc="GNOME application for managing PGP keys."
url="https://projects.gnome.org/seahorse/"
arch="all"
license="GPL-2.0-or-later"
depends="openssh-client dconf gnome-keyring gnupg-dirmngr"
makedepends="
	avahi-dev
	desktop-file-utils
	gcr-dev
	glib-dev
	gobject-introspection-dev
	gpgme-dev
	gtk+3.0-dev
	itstool
	libhandy1-dev
	libpwquality-dev
	libsecret-dev
	libsm-dev
	libsoup3-dev
	libxml2-utils
	meson
	openldap-dev
	vala
	yelp-tools
	"
checkdepends="appstream-glib"
subpackages="$pkgname-lang $pkgname-doc"
source="https://download.gnome.org/sources/seahorse/${pkgver%.*}/seahorse-$pkgver.tar.xz
	gnupg-2.4.patch
	stdout.patch
	avoid-c99-incompatibility.patch
	fix-werror-int-conversion.patch
	"

build() {
	abuild-meson \
		-Db_lto=true \
		. builddir
	meson compile -C builddir
}

check() {
	meson test --no-rebuild --print-errorlogs -C builddir
}

package() {
	DESTDIR="$pkgdir" meson install -C builddir
}

sha512sums="
168fdfc829134915f513028b1d35b647aa18a0390786cbf512f7ddb7b125b239f3f3d880a847119a5aa22580354f0b594e553fe3940a3afbda5861e69dd88e5d  seahorse-43.0.tar.xz
6e4f80ce8282fa462b72a69933beb92d88148888d15d9ed3eee6c4261b0844a4b353679358f122a630e39ead54fe439779c6c98d14d0bbb72b84935b382b050f  gnupg-2.4.patch
7ae7fecb2319333c9ad07b89453977e4a2bae9cc775b1f030bf70e35f6a57b0d93e818bcb4b51ce959fd6411d4015606c172cc487cddd04834bad643195ee6d5  stdout.patch
1ff1332843924ca9d4fca6f3b8c24207c83064f152f499c5a90a9cd74af3fa5ed95c8c10c935728d43f9867586b5af45aeae5d2a131110a4dd145d74ddf1abfa  avoid-c99-incompatibility.patch
443d70d09b24b4c3bfcf001d559b2d71c80600174db4f146180dbf8ac9186d85ceccc7d0db6bbb32183a21ea97d16471936643e8f22c4f402809a681472fdc5f  fix-werror-int-conversion.patch
"
