# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=libkomparediff2
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
pkgdesc="Library to compare files and strings"
url="https://kde.org/applications/development"
license="GPL-2.0-or-later AND LGPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	kio-dev
	kxmlgui-dev
	qt6-qtbase-dev
	samurai
	"
subpackages="$pkgname-dev $pkgname-lang"
_repo_url="https://invent.kde.org/sdk/libkomparediff2.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkomparediff2-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e4e29b47d1bdb42f5eb114184d1649e348b271b84e7968097790aba929b8c13fcada24f76dd367200bab0ca346c698f80458ae96601fd119c39d8d7bcf8298ac  libkomparediff2-24.08.0.tar.xz
"
