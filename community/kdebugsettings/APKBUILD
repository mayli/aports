# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kdebugsettings
pkgver=24.08.0
pkgrel=0
arch="all !armhf"
url="https://kde.org/applications/utilities/"
pkgdesc="An application to enable/disable qCDebug"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	kitemviews-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qt6-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/utilities/kdebugsettings.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdebugsettings-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
124b0369797d016ede149cd80a1b2c15d6a00bd282c0074ca3f8de012d729ef6b0149d370ed75b32dea622649cd0cb1f24c209d59ffe58c8e1e6366e2730dabe  kdebugsettings-24.08.0.tar.xz
"
