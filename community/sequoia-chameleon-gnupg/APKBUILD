# Maintainer: Simon Rupf <simon@rupf.net>
pkgname=sequoia-chameleon-gnupg
pkgver=0.11.0
pkgrel=0
pkgdesc="Sequoia's reimplementation of the GnuPG interface"
url="https://sequoia-pgp.org/"
arch="all"
license="GPL-3.0-or-later"
makedepends="
	bzip2-dev
	cargo
	cargo-auditable
	clang-dev
	nettle-dev
	openssl-dev
	sqlite-dev
	"
source="https://gitlab.com/sequoia-pgp/sequoia-chameleon-gnupg/-/archive/v$pkgver/sequoia-chameleon-gnupg-v$pkgver.tar.gz
	"
builddir="$srcdir/$pkgname-v$pkgver"
options="net !check" # bunch of failures against gpg cli

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/gpgv-sq target/release/gpg-sq \
		-t "$pkgdir"/usr/bin
}

sha512sums="
a1778330ef28d527dd2de189efd027da88fcdc41bd9d38f26f09fb46d4817c816401f6167d36c140a3e8898313c91d7c60043d56728346afae58165c3538aef7  sequoia-chameleon-gnupg-v0.11.0.tar.gz
"
