# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=grantleetheme
pkgver=24.08.0
pkgrel=0
pkgdesc="KDE PIM mail related libraries"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by knewstuff
arch="all !armhf !s390x !riscv64"
url="https://kontact.kde.org/"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends_dev="
	grantlee-dev
	ki18n-dev
	knewstuff-dev
	ktexttemplate-dev
	qt6-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	samurai
	"
checkdepends="xvfb-run"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/pim/grantleetheme.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/grantleetheme-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure -E "grantleetheme-grantleethemetest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
96dc3c583b18f2deaf6cf28387a513d1d16d2ce1d3b444111874ce0f6b544e8c760c77f75b64af91e8383f35e98a8aaaddd1e0100eb05e8d564af6ede088db17  grantleetheme-24.08.0.tar.xz
"
