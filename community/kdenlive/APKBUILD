# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kdenlive
pkgver=24.08.0
pkgrel=0
# ppc64le mlt uses 64bit long double while libgcc uses 128bit long double
# ppc64le, s390x, loongarch64 and riscv64 blocked by qt6-qtwebengine -> purpose
# aarch64 and armv7 blocked by OpenGL
arch="all !armhf !ppc64le !s390x !riscv64 !aarch64 !armv7 !loongarch64"
url="https://kdenlive.org"
pkgdesc="An intuitive and powerful multi-track video editor, including most recent video technologies"
license="GPL-2.0-or-later"
depends="
	ffmpeg
	frei0r-plugins
	qqc2-desktop-style
	"
makedepends="
	extra-cmake-modules
	kdeclarative-dev
	kdoctools-dev
	kfilemetadata-dev
	knewstuff-dev
	knotifyconfig-dev
	kxmlgui-dev
	mlt-dev
	purpose-dev
	qt6-qtbase-dev
	qt6-qtmultimedia-dev
	qt6-qtnetworkauth-dev
	qt6-qtsvg-dev
	rttr-dev
	samurai
	"
checkdepends="xvfb-run"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/multimedia/kdenlive.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdenlive-$pkgver.tar.xz"
options="!check" # Segfaulting

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
4179a36ff491cc7b7223b8dd5710b32dad3dd7737ca53452d76df4df61dfb9edeb396507ae446115c23366fc2891a7611afd161f81304f018e4d532036fb216d  kdenlive-24.08.0.tar.xz
"
