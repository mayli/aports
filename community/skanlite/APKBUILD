# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=skanlite
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/graphics/skanlite"
pkgdesc="Lite image scanning application"
license="LicenseRef-KDE-Accepted-GPL"
makedepends="
	extra-cmake-modules
	kcoreaddons-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kxmlgui-dev
	libksane-dev
	qt6-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/graphics/skanlite.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/skanlite-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DBUILD_WITH_QT6=ON \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
eb39a25e078df7904c5b573fe60a59f6732b8ce8b37e6e23a4616f89a0cce61a7d2282b3a827cc923c5a00c08a246524c38db6b4e7c7a5d57c0d3753d6263d93  skanlite-24.08.0.tar.xz
"
