# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=py3-sphinx-autodoc-typehints
_pyname=sphinx-autodoc-typehints
pkgver=2.4.0
pkgrel=0
pkgdesc="Type hints support for the Sphinx autodoc extension"
url="https://github.com/tox-dev/sphinx-autodoc-typehints"
arch="noarch"
license="MIT"
options="net"
depends="python3 py3-sphinx py3-typing-extensions"
makedepends="py3-gpep517 py3-installer py3-hatchling py3-hatch-vcs"
checkdepends="py3-pytest py3-sphobjinv py3-nptyping"
subpackages="$pkgname-pyc"
source="$_pyname-$pkgver.tar.gz::https://github.com/tox-dev/sphinx-autodoc-typehints/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/$_pyname-$pkgver"

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages test-env
	test-env/bin/python3 -m installer .dist/sphinx_autodoc_typehints*.whl
	# Test fails: https://github.com/tox-dev/sphinx-autodoc-typehints/issues/450
	test-env/bin/python3 -m pytest \
		--deselect 'tests/test_sphinx_autodoc_typehints.py::test_always_document_param_types[doc_param_type]'
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/sphinx_autodoc_typehints*.whl
}

sha512sums="
8279ce47b242f5b056598fa6d45a6cfab3e58e1b5421c150ac41c9c89e4fd72039383b105a712f7cb4246c0054c7e2a83843969475747544fce59bd8ae49ed08  sphinx-autodoc-typehints-2.4.0.tar.gz
"
